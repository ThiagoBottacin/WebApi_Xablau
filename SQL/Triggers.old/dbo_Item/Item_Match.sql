USE [Xablau]
GO
/****** Object:  Trigger [dbo].[Item_Match]    Script Date: 12/9/2017 4:26:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[Item_Match]
ON [dbo].[Item]
AFTER INSERT, UPDATE 
AS
   EXEC dbo.sp_Match
