USE [Xablau]
GO
/****** Object:  StoredProcedure [dbo].[sp_Match]    Script Date: 12/14/2017 1:19:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[sp_Match]
AS
	SET NOCOUNT ON;

	SELECT DISTINCT it1.ItemId AS ITEM1, it2.ItemId AS ITEM2 into #FirstCombination FROM ItemTag it1
			JOIN ItemTag it2
		ON it1.TagTypeId = 2 and it2.TagTypeId = 1
		INNER JOIN Item i ON it1.ItemId = i.ItemId
		INNER JOIN Item i2 ON it2.ItemId = i2.ItemId
	WHERE it1.ItemId <> it2.ItemId AND i.PersonId <> i2.PersonId AND i.DataDelete IS NULL AND i2.DataDelete IS NULL
	AND (it1.TagId = it2.TagId )
	AND it1.ItemId < it2.ItemId

	SELECT DISTINCT it1.ItemId AS ITEM1, it2.ItemId AS ITEM2 into #SecondCombination FROM ItemTag it1
			JOIN ItemTag it2
		ON it1.TagTypeId = 1 and it2.TagTypeId = 2
		INNER JOIN Item i ON it1.ItemId = i.ItemId
		INNER JOIN Item i2 ON it2.ItemId = i2.ItemId
	WHERE it1.ItemId <> it2.ItemId AND i.PersonId <> i2.PersonId AND i.DataDelete IS NULL AND i2.DataDelete IS NULL
	AND (it1.TagId = it2.TagId )
	AND it1.ItemId < it2.ItemId

	INSERT INTO Change (Item1Id, Item2Id, DataSave)
		SELECT DISTINCT it1.ItemId, it2.ItemId, GETDATE() FROM ItemTag it1
			JOIN ItemTag it2
				ON it1.TagId = it2.TagId AND it1.TagTypeId <> it2.TagTypeId AND it1.itemId <> it2.itemId
				AND it1.ItemId < it2.ItemId
		WHERE 
		(CONCAT(it1.ItemId, it2.ItemId) IN (SELECT CONCAT(ITEM1, ITEM2) FROM #FirstCombination) AND CONCAT(it1.ItemId, it2.ItemId) IN (SELECT CONCAT(ITEM1, ITEM2) FROM #SecondCombination))
		AND (CONCAT(it1.ItemId, it2.ItemId) NOT IN(SELECT CONCAT(Item1Id, Item2Id) FROM Change WHERE DataDelete IS NULL) AND CONCAT(it1.ItemId, it2.ItemId) NOT IN(SELECT CONCAT(Item2Id, Item1Id) FROM Change WHERE DataDelete IS NULL));

	DROP TABLE #FirstCombination
	DROP TABLE #SecondCombination

