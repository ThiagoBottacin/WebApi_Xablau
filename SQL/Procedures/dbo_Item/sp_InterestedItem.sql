USE [Xablau]
GO
/****** Object:  StoredProcedure [dbo].[sp_InterestedItem]    Script Date: 12/14/2017 1:23:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[sp_InterestedItem] @userId INT
AS
	SET NOCOUNT ON;

	SELECT DISTINCT it1.ItemId AS ITEM1, it2.ItemId AS ITEM2 INTO #MeDesejam FROM ItemTag it1
			JOIN Item i on i.PersonId = @userId AND i.ItemId = it1.ItemId AND i.DataDelete IS NULL
			JOIN ItemTag it2
			JOIN Item i2 on i2.PersonId <> @userId AND i2.ItemId = it2.ItemId AND i2.DataDelete IS NULL
		ON it1.TagTypeId = 1 and it2.TagTypeId = 2
	WHERE it1.ItemId <> it2.ItemId
	AND (it1.TagId = it2.TagId )
	

	SELECT DISTINCT it1.ItemId AS ITEM1, it2.ItemId AS ITEM2 INTO #MeusDesejos FROM ItemTag it1
			JOIN Item i on i.PersonId = @userId AND i.ItemId = it1.ItemId AND i.DataDelete IS NULL
			JOIN ItemTag it2
			JOIN Item i2 on i2.PersonId <> @userId AND i2.ItemId = it2.ItemId AND i2.DataDelete IS NULL
		ON it1.TagTypeId = 2 and it2.TagTypeId = 1
	WHERE it1.ItemId <> it2.ItemId
	AND (it1.TagId = it2.TagId )
	

	SELECT * FROM Item WHERE ItemId IN ( 
		SELECT DISTINCT it2.ItemId FROM ItemTag it1
			JOIN Item i on i.PersonId = @userId AND i.ItemId = it1.ItemId
			JOIN ItemTag it2
			JOIN Item i2 on i2.PersonId <> @userId AND i2.ItemId = it2.ItemId
				ON it1.itemId <> it2.itemId
		WHERE 
		(CONCAT(it1.ItemId, it2.ItemId) IN (SELECT CONCAT(ITEM1, ITEM2) FROM #MeDesejam) AND CONCAT(it1.ItemId, it2.ItemId) NOT IN (SELECT CONCAT(ITEM1, ITEM2) FROM #MeusDesejos))
		AND (CONCAT(it1.ItemId, it2.ItemId) NOT IN(SELECT CONCAT(Item1Id, Item2Id) FROM Change WHERE DataDelete IS NULL) AND CONCAT(it1.ItemId, it2.ItemId) NOT IN (SELECT CONCAT(Item2Id, Item1Id) FROM Change WHERE DataDelete IS NULL)))
	AND Item.DataDelete IS NULL;

	DROP TABLE #MeDesejam
	DROP TABLE #MeusDesejos

