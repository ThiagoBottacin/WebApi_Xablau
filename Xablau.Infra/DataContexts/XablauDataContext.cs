﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Xablau.Domain.Entities;
using Xablau.Infra.Mappings;


namespace Xablau.Infra.DataContexts
{
    public class XablauDataContext : DbContext
    {
        public XablauDataContext()
            : base("XablauConnectionString")
        {
            Database.SetInitializer<XablauDataContext>(new XablauDataContextInitializer());
            //Configuration.LazyLoadingEnabled = false;
            //Configuration.ProxyCreationEnabled = false;
        }
        
        public IDbSet<Person> People{ get; set; }
        public IDbSet<Address> Addresses { get; set; }
        public IDbSet<Phone> Phones { get; set; }
        public IDbSet<Item> Items { get; set; }
        public IDbSet<Tag> Tags { get; set; }
        public IDbSet<TagType> TagTypes { get; set; }
        public IDbSet<ItemTag> ItemTag { get; set; }
        public IDbSet<Picture> Pictures { get; set; }
        public IDbSet<Change> Changes { get; set; }
        public IDbSet<Message> Messages { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
                       
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            //modelBuilder.Properties().Where(p => p.Name == p.ReflectedType.Name + "Id").Configure(p => p.IsKey());
            modelBuilder.Properties<string>().Configure(x => x.HasColumnType("nvarchar"));

            modelBuilder.Configurations.Add(new PersonMap());
            modelBuilder.Configurations.Add(new AddressMap());
            modelBuilder.Configurations.Add(new PhoneMap());
            modelBuilder.Configurations.Add(new ItemMap());
            modelBuilder.Configurations.Add(new TagMap());
            modelBuilder.Configurations.Add(new TagTypeMap());
            modelBuilder.Configurations.Add(new ItemTagMap());
            modelBuilder.Configurations.Add(new PictureMap());
            modelBuilder.Configurations.Add(new ChangeMap());
            modelBuilder.Configurations.Add(new MessageMap());

            base.OnModelCreating(modelBuilder);
        }
    }

    public class XablauDataContextInitializer : DropCreateDatabaseIfModelChanges<XablauDataContext>
    {

        protected override void Seed(XablauDataContext context)
        {

            context.People.Add(new Person { PersonId = 1, Name = "Caique", Email = "caique@gmail.com", Password = "40bd001563085fc35165329ea1ff5c5ecbdbbeef", Photo = null, DtBorn = DateTime.Now.AddYears(-20), DataDelete = null, DataSave = DateTime.Now });
            context.People.Add(new Person { PersonId = 2, Name = "Maria", Email = "maria@gmail.com", Password = "40bd001563085fc35165329ea1ff5c5ecbdbbeef", Photo = null, DtBorn = DateTime.Now.AddYears(-40), DataDelete = null, DataSave = DateTime.Now });
            context.People.Add(new Person { PersonId = 3, Name = "Thiago", Email = "thiago@gmail.com", Password = "40bd001563085fc35165329ea1ff5c5ecbdbbeef", Photo = null, DtBorn = DateTime.Now.AddYears(-22), DataDelete = null, DataSave = DateTime.Now });
            context.People.Add(new Person { PersonId = 4, Name = "Juliana", Email = "juliana@gmail.com", Password = "40bd001563085fc35165329ea1ff5c5ecbdbbeef", Photo = null, DtBorn = DateTime.Now.AddYears(-35), DataDelete = null, DataSave = DateTime.Now });

            //TagType
            context.TagTypes.Add(new TagType { TagTypeId = 1, Name = "descricao" });
            context.TagTypes.Add(new TagType { TagTypeId = 2, Name = "desejo" });

            //Tag
            context.Tags.Add(new Tag { TagId = 1, Name = "Geladeira" });
            context.Tags.Add(new Tag { TagId = 2, Name = "Fogão" });

            //Item
            context.Items.Add(new Item { ItemId = 1, Name = "Geladeira", PersonId = 1, DataDelete = null, DataSave = DateTime.Now, Description = "Geladeira", Price = null });
            context.Items.Add(new Item { ItemId = 2, Name = "Fogão", PersonId = 2, DataDelete = null, DataSave = DateTime.Now, Description = "Fogão", Price = null });

            //ItemTag
            context.ItemTag.Add(new ItemTag { ItemTagId = 1, TagId = 1, ItemId = 1, TagTypeId = 1 });
            context.ItemTag.Add(new ItemTag { ItemTagId = 2, TagId = 2, ItemId = 1, TagTypeId = 2 });
            context.ItemTag.Add(new ItemTag { ItemTagId = 3, TagId = 2, ItemId = 2, TagTypeId = 1 });
            context.ItemTag.Add(new ItemTag { ItemTagId = 4, TagId = 1, ItemId = 2, TagTypeId = 2 });

            context.SaveChanges();
            base.Seed(context);
        }
    }
}