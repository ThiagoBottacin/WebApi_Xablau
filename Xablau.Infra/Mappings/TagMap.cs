﻿using System.Data.Entity.ModelConfiguration;
using Xablau.Domain.Entities;

namespace Xablau.Infra.Mappings
{
    public class TagMap : EntityTypeConfiguration<Tag>
    {
        public TagMap()
        {
            this.ToTable("Tag");

            this.HasKey(x => x.TagId);

            this.Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(50);
        }
    }
}