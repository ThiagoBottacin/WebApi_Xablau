﻿using System.Data.Entity.ModelConfiguration;
using Xablau.Domain.Entities;

namespace Xablau.Infra.Mappings
{
    public class ItemMap : EntityTypeConfiguration<Item>
    {
        public ItemMap()
        {
            this.ToTable("Item");

            this.HasKey(x => x.ItemId);

            this.Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(x => x.Description)
                .IsRequired()
                .HasMaxLength(2000);

            this.Property(x => x.Price);

            this.HasRequired(p => p.Person)
            .WithMany()
            .HasForeignKey(p => p.PersonId);
        }
    }
}