﻿using System.Data.Entity.ModelConfiguration;
using Xablau.Domain.Entities;

namespace Xablau.Infra.Mappings
{
    public class AddressMap : EntityTypeConfiguration<Address>
    {
        public AddressMap()
        {
            this.ToTable("Address");

            this.HasKey(x => x.AddressId);

            this.Property(x => x.Country)
                .IsRequired()
                .HasMaxLength(25);

            this.Property(x => x.State)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(x => x.City)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(x => x.District)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(x => x.Street)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(x => x.Number)
                .IsRequired()
                .HasMaxLength(10);

            this.HasRequired(p => p.Person)
            .WithMany()
            .HasForeignKey(p => p.PersonId);
        }
    }
}