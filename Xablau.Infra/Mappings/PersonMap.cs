﻿using System.Data.Entity.ModelConfiguration;
using Xablau.Domain.Entities;

namespace Xablau.Infra.Mappings
{
    public class PersonMap : EntityTypeConfiguration<Person>
    {
        public PersonMap()
        {
            this.ToTable("Person");

            this.HasKey(x => x.PersonId);

            this.Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(x => x.Email)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(x => x.Password)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(x => x.DtBorn).IsRequired();

            this.Property(x => x.DataDelete);

            this.Property(x => x.Photo)
                .HasMaxLength(100);
        }
    }
}