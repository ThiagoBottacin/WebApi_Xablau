﻿using System.Data.Entity.ModelConfiguration;
using Xablau.Domain.Entities;

namespace Xablau.Infra.Mappings
{
    public class ItemTagMap : EntityTypeConfiguration<ItemTag>
    {
        public ItemTagMap()
        {
            this.ToTable("ItemTag");

            this.HasKey(x => x.ItemTagId);

            this.HasRequired(p => p.Item)
            .WithMany()
            .HasForeignKey(p => p.ItemId);

            this.HasRequired(p => p.Tag)
            .WithMany()
            .HasForeignKey(p => p.TagId);

            this.HasRequired(p => p.TagType)
            .WithMany()
            .HasForeignKey(p => p.TagTypeId);
        }
    }
}