﻿using System.Data.Entity.ModelConfiguration;
using Xablau.Domain.Entities;

namespace Xablau.Infra.Mappings
{
    public class PhoneMap : EntityTypeConfiguration<Phone>
    {
        public PhoneMap()
        {
            this.ToTable("Phone");

            this.HasKey(x => x.PhoneId);

            this.Property(x => x.DDD)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(x => x.Number)
                .IsRequired()
                .HasMaxLength(15);

            this.HasRequired(p => p.Person)
            .WithMany()
            .HasForeignKey(p => p.PersonId);
        }
    }
}