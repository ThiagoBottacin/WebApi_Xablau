﻿using System.Data.Entity.ModelConfiguration;
using Xablau.Domain.Entities;

namespace Xablau.Infra.Mappings
{
    public class PictureMap : EntityTypeConfiguration<Picture>
    {
        public PictureMap()
        {
            this.ToTable("Picture");

            this.HasKey(x => x.PictureId); 

            this.Property(x => x.Image)
                .IsRequired()
                .HasMaxLength(200);

            this.HasRequired(p => p.Item)
            .WithMany()
            .HasForeignKey(p => p.ItemId);
        }
    }
}