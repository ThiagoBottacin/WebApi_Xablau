﻿using System.Data.Entity.ModelConfiguration;
using Xablau.Domain.Entities;

namespace Xablau.Infra.Mappings
{
    public class TagTypeMap : EntityTypeConfiguration<TagType>
    {
        public TagTypeMap()
        {
            this.ToTable("TagType");

            this.HasKey(x => x.TagTypeId);

            this.Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(50);
        }
    }
}