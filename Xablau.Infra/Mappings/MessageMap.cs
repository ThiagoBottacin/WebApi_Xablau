﻿using System.Data.Entity.ModelConfiguration;
using Xablau.Domain.Entities;
namespace Xablau.Infra.Mappings
{
    public class MessageMap : EntityTypeConfiguration<Message>
    {
        public MessageMap()
        {
            this.ToTable("Message");

            this.HasKey(x => x.MessageId);

            this.Property(x => x.Text)
                .IsRequired()
                .HasMaxLength(1000);

            this.HasRequired(p => p.Person)
            .WithMany()
            .HasForeignKey(p => p.PersonId);

            this.HasRequired(p => p.Change)
            .WithMany()
            .HasForeignKey(p => p.ChangeId);
        }
    }
}