﻿using System.Data.Entity.ModelConfiguration;
using Xablau.Domain.Entities;

namespace Xablau.Infra.Mappings
{
    public class ChangeMap : EntityTypeConfiguration<Change>
    {
        public ChangeMap()
        {
            this.ToTable("Change");

            this.HasKey(x => x.ChangeId);

            this.HasRequired(p => p.Item1)
            .WithMany()
            .HasForeignKey(p => p.Item1Id);

            this.HasRequired(p => p.Item2)
            .WithMany()
            .HasForeignKey(p => p.Item2Id);
        }
    }
}
