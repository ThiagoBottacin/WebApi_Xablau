﻿using Xablau.Domain.Entities;
using Xablau.Domain.Repositories;

namespace Xablau.Infra.Repositories
{
    public class ItemTagRepository : RepositoryBase<ItemTag>, IItemTagRepository
    {
    }
}
