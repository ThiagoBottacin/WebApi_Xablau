﻿using Xablau.Domain.Entities;
using Xablau.Domain.Repositories;

namespace Xablau.Infra.Repositories
{
    public class TagTypeRepository : RepositoryBase<TagType>, ITagTypeRepository
    {
    }
}
