﻿using Xablau.Domain.Entities;
using Xablau.Domain.Repositories;

namespace Xablau.Infra.Repositories
{
    public class ChangeRepository : RepositoryBase<Change>, IChangeRepository
    {

        public void Match()
        {

            Db.Database.ExecuteSqlCommand("Exec sp_Match");
        }
    }
}