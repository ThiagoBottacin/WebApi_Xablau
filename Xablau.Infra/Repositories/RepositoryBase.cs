﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;
using Xablau.Domain.Repositories;
using Xablau.Infra.DataContexts;

namespace Xablau.Infra.Repositories
{
    public class RepositoryBase<TEntity> : IDisposable, IRepositoryBase<TEntity> where TEntity : class
    {

        protected XablauDataContext Db = new XablauDataContext();
        internal DbSet<TEntity> dbSet;

        public void Add(TEntity obj)
        {
            Db.Set<TEntity>().Add(obj);
            Db.SaveChanges();
        }

        public TEntity GetById(int id)
        {
            return Db.Set<TEntity>().Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return Db.Set<TEntity>().ToList();
        }

        public void Update(TEntity obj)
        {
            Db.Entry(obj).State = EntityState.Modified;
            Db.SaveChanges();
        }

        public void Remove(TEntity obj)
        {
            Db.Set<TEntity>().Remove(obj);
            Db.SaveChanges();
        }

        //public void Remove(object id)
        //{
        //    TEntity entityToDelete = dbSet.Find(id);
        //    Remove(entityToDelete);
        //}
                
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public TEntity Get(Func<TEntity, bool> expr)
        {
            return Db.Set<TEntity>().FirstOrDefault(expr);
        }

        public IEnumerable<TEntity> GetAll(Func<TEntity, bool> expr)
        {
            return Db.Set<TEntity>().ToList().Where(expr);
        }

        //Get Queryable
        public IQueryable<TEntity> GetQueryable(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "")
        {
            IQueryable<TEntity> query = Db.Set<TEntity>();
            
            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                query = orderBy(query);
            }
            if (query != null && !String.IsNullOrEmpty(query.ToString()))
            {
                String str = System.Text.RegularExpressions.Regex.Replace(query.ToString(), Environment.NewLine, "");
            }

            return query;
        }
    }
}
