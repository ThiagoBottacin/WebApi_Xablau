﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Xablau.Domain.Entities;
using Xablau.Domain.Repositories;

namespace Xablau.Infra.Repositories
{
    public class ItemRepository : RepositoryBase<Item>, IItemRepository
    {

        public IEnumerable<Item> GetInterestedItems(int userId)
        {
            SqlParameter userIdParam = new SqlParameter("@userId", userId);

            var items = Db.Database.SqlQuery<Item>("EXEC sp_InterestedItem @userId", userIdParam).ToList();
            return items;
        }
    }
}
