using Xablau.Domain.Entities;

namespace Xablau.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<Xablau.Infra.DataContexts.XablauDataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Xablau.Infra.DataContexts.XablauDataContext context)
        {

            context.People.AddOrUpdate(new Person { PersonId = 1, Name = "Caique", Email = "caique@gmail.com", Password = "40bd001563085fc35165329ea1ff5c5ecbdbbeef", Photo = null, DtBorn = DateTime.Now.AddYears(-20), DataDelete = null, DataSave = DateTime.Now });
            context.People.AddOrUpdate(new Person { PersonId = 2, Name = "Maria", Email = "maria@gmail.com", Password = "40bd001563085fc35165329ea1ff5c5ecbdbbeef", Photo = null, DtBorn = DateTime.Now.AddYears(-40), DataDelete = null, DataSave = DateTime.Now });
            context.People.AddOrUpdate(new Person { PersonId = 3, Name = "Thiago", Email = "thiago@gmail.com", Password = "40bd001563085fc35165329ea1ff5c5ecbdbbeef", Photo = null, DtBorn = DateTime.Now.AddYears(-22), DataDelete = null, DataSave = DateTime.Now });
            context.People.AddOrUpdate(new Person { PersonId = 4, Name = "Juliana", Email = "juliana@gmail.com", Password = "40bd001563085fc35165329ea1ff5c5ecbdbbeef", Photo = null, DtBorn = DateTime.Now.AddYears(-35), DataDelete = null, DataSave = DateTime.Now });

            //TagType
            context.TagTypes.AddOrUpdate(new TagType { TagTypeId = 1, Name = "descricao" });
            context.TagTypes.AddOrUpdate(new TagType { TagTypeId = 2, Name = "desejo" });

            //Tag
            context.Tags.AddOrUpdate(new Tag { TagId = 1, Name = "Geladeira" });
            context.Tags.AddOrUpdate(new Tag { TagId = 2, Name = "Fog�o" });

            //Item
            context.Items.AddOrUpdate(new Item { ItemId = 1, Name = "Geladeira", PersonId = 1, DataDelete = null, DataSave = DateTime.Now, Description = "Geladeira", Price = null });
            context.Items.AddOrUpdate(new Item { ItemId = 2, Name = "Fog�o", PersonId = 2, DataDelete = null, DataSave = DateTime.Now, Description = "Fog�o", Price = null });

            //ItemTag
            context.ItemTag.AddOrUpdate(new ItemTag { ItemTagId = 1, TagId = 1, ItemId = 1, TagTypeId = 1 });
            context.ItemTag.AddOrUpdate(new ItemTag { ItemTagId = 2, TagId = 2, ItemId = 1, TagTypeId = 2 });
            context.ItemTag.AddOrUpdate(new ItemTag { ItemTagId = 3, TagId = 2, ItemId = 2, TagTypeId = 1 });
            context.ItemTag.AddOrUpdate(new ItemTag { ItemTagId = 4, TagId = 1, ItemId = 2, TagTypeId = 2 });

            context.SaveChanges();
            base.Seed(context);
        }
    }
}