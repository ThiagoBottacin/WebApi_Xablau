namespace Xablau.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migrationAzure : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Address",
                c => new
                    {
                        AddressId = c.Int(nullable: false, identity: true),
                        Country = c.String(nullable: false, maxLength: 25),
                        State = c.String(nullable: false, maxLength: 50),
                        City = c.String(nullable: false, maxLength: 50),
                        District = c.String(nullable: false, maxLength: 50),
                        Street = c.String(nullable: false, maxLength: 50),
                        Number = c.String(nullable: false, maxLength: 10),
                        ZipCode = c.String(maxLength: 4000),
                        DataSave = c.DateTime(nullable: false),
                        DataDelete = c.DateTime(),
                        PersonId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AddressId)
                .ForeignKey("dbo.Person", t => t.PersonId)
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.Person",
                c => new
                    {
                        PersonId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Email = c.String(nullable: false, maxLength: 100),
                        Password = c.String(nullable: false, maxLength: 50),
                        DtBorn = c.DateTime(nullable: false),
                        Photo = c.String(maxLength: 100),
                        DataSave = c.DateTime(nullable: false),
                        DataDelete = c.DateTime(),
                    })
                .PrimaryKey(t => t.PersonId);
            
            CreateTable(
                "dbo.Change",
                c => new
                    {
                        ChangeId = c.Int(nullable: false, identity: true),
                        Item1Id = c.Int(nullable: false),
                        Item2Id = c.Int(nullable: false),
                        Price_1 = c.Decimal(precision: 18, scale: 2),
                        Price_2 = c.Decimal(precision: 18, scale: 2),
                        Status = c.Boolean(),
                        Like1 = c.Boolean(),
                        Like2 = c.Boolean(),
                        DataSave = c.DateTime(nullable: false),
                        DataDelete = c.DateTime(),
                    })
                .PrimaryKey(t => t.ChangeId)
                .ForeignKey("dbo.Item", t => t.Item1Id)
                .ForeignKey("dbo.Item", t => t.Item2Id)
                .Index(t => t.Item1Id)
                .Index(t => t.Item2Id);
            
            CreateTable(
                "dbo.Item",
                c => new
                    {
                        ItemId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(nullable: false, maxLength: 2000),
                        Price = c.Decimal(precision: 18, scale: 2),
                        DataSave = c.DateTime(nullable: false),
                        DataDelete = c.DateTime(),
                        PersonId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId)
                .ForeignKey("dbo.Person", t => t.PersonId)
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.ItemTag",
                c => new
                    {
                        ItemTagId = c.Int(nullable: false, identity: true),
                        ItemId = c.Int(nullable: false),
                        TagId = c.Int(nullable: false),
                        TagTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ItemTagId)
                .ForeignKey("dbo.Item", t => t.ItemId)
                .ForeignKey("dbo.Tag", t => t.TagId)
                .ForeignKey("dbo.TagType", t => t.TagTypeId)
                .Index(t => t.ItemId)
                .Index(t => t.TagId)
                .Index(t => t.TagTypeId);
            
            CreateTable(
                "dbo.Tag",
                c => new
                    {
                        TagId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.TagId);
            
            CreateTable(
                "dbo.TagType",
                c => new
                    {
                        TagTypeId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.TagTypeId);
            
            CreateTable(
                "dbo.Message",
                c => new
                    {
                        MessageId = c.Int(nullable: false, identity: true),
                        Text = c.String(nullable: false, maxLength: 1000),
                        DataSave = c.DateTime(nullable: false),
                        DataDelete = c.DateTime(),
                        PersonId = c.Int(nullable: false),
                        ChangeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MessageId)
                .ForeignKey("dbo.Change", t => t.ChangeId)
                .ForeignKey("dbo.Person", t => t.PersonId)
                .Index(t => t.PersonId)
                .Index(t => t.ChangeId);
            
            CreateTable(
                "dbo.Phone",
                c => new
                    {
                        PhoneId = c.Int(nullable: false, identity: true),
                        DDD = c.String(nullable: false, maxLength: 10),
                        Number = c.String(nullable: false, maxLength: 15),
                        DataSave = c.DateTime(nullable: false),
                        DataDelete = c.DateTime(),
                        PersonId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PhoneId)
                .ForeignKey("dbo.Person", t => t.PersonId)
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.Picture",
                c => new
                    {
                        PictureId = c.Int(nullable: false, identity: true),
                        Image = c.String(nullable: false, maxLength: 200),
                        DataSave = c.DateTime(nullable: false),
                        DataDelete = c.DateTime(),
                        ItemId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PictureId)
                .ForeignKey("dbo.Item", t => t.ItemId)
                .Index(t => t.ItemId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Picture", "ItemId", "dbo.Item");
            DropForeignKey("dbo.Phone", "PersonId", "dbo.Person");
            DropForeignKey("dbo.Message", "PersonId", "dbo.Person");
            DropForeignKey("dbo.Message", "ChangeId", "dbo.Change");
            DropForeignKey("dbo.ItemTag", "TagTypeId", "dbo.TagType");
            DropForeignKey("dbo.ItemTag", "TagId", "dbo.Tag");
            DropForeignKey("dbo.ItemTag", "ItemId", "dbo.Item");
            DropForeignKey("dbo.Change", "Item2Id", "dbo.Item");
            DropForeignKey("dbo.Change", "Item1Id", "dbo.Item");
            DropForeignKey("dbo.Item", "PersonId", "dbo.Person");
            DropForeignKey("dbo.Address", "PersonId", "dbo.Person");
            DropIndex("dbo.Picture", new[] { "ItemId" });
            DropIndex("dbo.Phone", new[] { "PersonId" });
            DropIndex("dbo.Message", new[] { "ChangeId" });
            DropIndex("dbo.Message", new[] { "PersonId" });
            DropIndex("dbo.ItemTag", new[] { "TagTypeId" });
            DropIndex("dbo.ItemTag", new[] { "TagId" });
            DropIndex("dbo.ItemTag", new[] { "ItemId" });
            DropIndex("dbo.Item", new[] { "PersonId" });
            DropIndex("dbo.Change", new[] { "Item2Id" });
            DropIndex("dbo.Change", new[] { "Item1Id" });
            DropIndex("dbo.Address", new[] { "PersonId" });
            DropTable("dbo.Picture");
            DropTable("dbo.Phone");
            DropTable("dbo.Message");
            DropTable("dbo.TagType");
            DropTable("dbo.Tag");
            DropTable("dbo.ItemTag");
            DropTable("dbo.Item");
            DropTable("dbo.Change");
            DropTable("dbo.Person");
            DropTable("dbo.Address");
        }
    }
}
