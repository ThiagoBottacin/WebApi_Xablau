﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Xablau.Application.Interfaces;
using Xablau.Domain.Entities;

namespace Xablau.API.Controllers
{
    [RoutePrefix("api/phone")]
    public class PhoneController : ApiController
    {
        #region Services
        private readonly IPhoneAppService _phoneAppService;

        public PhoneController(IPhoneAppService phoneAppService)
        {
            _phoneAppService = phoneAppService;
        } 
        #endregion

        [Authorize(Roles = "User")]
        [HttpPost]
        [Route("alter-phone")]
        public HttpResponseMessage AlterPhone(Phone phone)
        {

            int userId = int.Parse(User.Identity.Name);

            var userPhone = _phoneAppService.Get(p => p.PersonId == userId);

            bool alter = false;

            if (userPhone == null)
            {
                Phone newphone = new Phone
                {
                    PersonId = userId,
                    DDD = phone.DDD,
                    Number = phone.Number,
                    DataSave = DateTime.Now,
                    DataDelete = null
                };

                _phoneAppService.Add(newphone);

                return Request.CreateResponse(HttpStatusCode.Accepted, "Success");
            }

            if (phone.DDD != userPhone.DDD)
            {
                userPhone.DDD = phone.DDD;
                alter = true;
            }

            if (phone.Number != userPhone.Number)
            {
                userPhone.Number = phone.Number;
                alter = true;
            }

            if (alter)
            {
                try
                {
                    _phoneAppService.Update(userPhone);
                    return Request.CreateResponse(HttpStatusCode.Accepted, "Sucess");
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }

            return Request.CreateResponse(HttpStatusCode.Forbidden, "Forbidden"); 
        }
    }
}