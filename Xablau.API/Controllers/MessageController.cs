﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Xablau.Application.Interfaces;
using Xablau.Domain.Entities;

namespace Xablau.API.Controllers
{
    [RoutePrefix("api/message")]
    public class MessageController : ApiController
    {

        #region Services

        private readonly IMessageAppService _messageAppService;
        private readonly IChangeAppService _changeAppService;

        public MessageController(IMessageAppService messageAppService, IChangeAppService changeAppService)
        {
            _messageAppService = messageAppService;
            _changeAppService = changeAppService;
        }

        #endregion


        [Authorize(Roles = "User")]
        [HttpGet]
        [Route("")]
        public HttpResponseMessage Message(int changeId)
        {
            
            int userId = int.Parse(User.Identity.Name);

            try
            {
                var result = _messageAppService.GetQueryable(m => m.DataDelete == null && (m.Change.Item1.PersonId == userId || m.Change.Item2.PersonId == userId) && m.ChangeId == changeId)
                    .Select(s => new
                {
                    s.Person.Name,
                    s.Text,
                    Date = s.DataSave,
                    IsOwner = s.PersonId == userId
                }).ToList();

                return Request.CreateResponse(HttpStatusCode.Accepted, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [Authorize(Roles = "User")]
        [HttpPost]
        [Route("")]
        public void Message(Message message)
        {
            if (string.IsNullOrEmpty(message.Text))
                return;

            int userId = int.Parse(User.Identity.Name);

            var result = _changeAppService.GetQueryable(q => q.DataDelete == null && q.ChangeId == message.ChangeId && (q.Item1.PersonId == userId || q.Item2.PersonId == userId)).FirstOrDefault();

            if (result != null)
            {
                message.PersonId = userId;
                message.DataSave = DateTime.Now;
                message.DataDelete = null;

                _messageAppService.Add(message);
            }
        }
    }
}