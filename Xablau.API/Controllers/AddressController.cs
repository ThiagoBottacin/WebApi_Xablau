﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Xablau.Application.Interfaces;
using Xablau.Domain.Entities;

namespace Xablau.API.Controllers
{
    [RoutePrefix("api/address")]
    public class AddressController : ApiController
    {

        #region Services

        private readonly IAddressAppService _addressAppService;

        public AddressController(IAddressAppService addressAppService)
        {
            _addressAppService = addressAppService;
        }

        #endregion


        [HttpGet]
        [Route("{query=query}")]
        public HttpResponseMessage LocalityNames(string query)
        {
            try
            {
                var result = _addressAppService.GetNamesLocality(query);

                return Request.CreateResponse(HttpStatusCode.Accepted, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }


        [Authorize(Roles = "User")]
        [HttpPost]
        [Route("add")]
        public HttpResponseMessage Insert(Address address)
        {
            try
            {
                Address newAddress = new Address
                {
                    City = address.City,
                    State = address.State,
                    District = address.District,
                    Street = address.Street,
                    Number = address.Number,
                    ZipCode = address.ZipCode,
                    DataSave = DateTime.Now,
                    DataDelete = null,
                    PersonId = int.Parse(User.Identity.Name)
                };

                _addressAppService.Add(newAddress);

                return Request.CreateResponse(HttpStatusCode.Accepted, "Sucess");
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid");
            }
        }


        [Authorize(Roles = "User")]
        [HttpDelete]
        [Route("")]
        public HttpResponseMessage Delete(int addressId)
        {
            var address = _addressAppService.Get(addressId, int.Parse(User.Identity.Name));

            if (address == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Falha na operação.");

            address.DataDelete = DateTime.Now;
            _addressAppService.Update(address);

            return Request.CreateResponse(HttpStatusCode.Accepted, "Sucess");
        }


        [Authorize(Roles = "User")]
        [HttpGet]
        [Route("")]
        public IEnumerable<object> Address()
        {

            int userId = int.Parse(User.Identity.Name);

            var result = _addressAppService.GetQueryable(x => x.PersonId == userId && x.DataDelete == null).Select(s =>
                new
                {
                    s.AddressId,
                    s.City,
                    s.Country,
                    s.State,
                    s.District,
                    s.Street,
                    s.Number,
                    s.ZipCode
                }).ToList();

            return result;
        }
    }
}