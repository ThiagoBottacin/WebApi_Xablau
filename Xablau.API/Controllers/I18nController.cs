﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;

namespace Xablau.API.Controllers
{
    [RoutePrefix("api/i18n")]
    public class I18nController : ApiController
    {
        [HttpGet]
        [Route("")]
        public HttpResponseMessage Get(string lang)
        {
            try
            {
                var filePath = HttpContext.Current.Server.MapPath($"~/i18n/locales/{lang}/translation.json");

                if (!File.Exists(filePath))
                    filePath = HttpContext.Current.Server.MapPath("~/i18n/locales/pt-BR/translation.json");

                var fileAsString = File.ReadAllText(filePath);

                var fileAsJson = JsonConvert.DeserializeObject(fileAsString);

                return Request.CreateResponse(HttpStatusCode.Accepted, fileAsJson);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.Accepted, e.Message);
            }
        }
    }
}