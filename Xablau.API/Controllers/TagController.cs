﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Xablau.Application.Interfaces;

namespace Xablau.API.Controllers
{
    [RoutePrefix("api/tags")]
    public class TagController : ApiController
    {
        #region Services
       
        private readonly ITagAppService _tagService;

        public TagController(ITagAppService tagService)
        {
            _tagService = tagService;
        }

        #endregion

        [HttpGet]
        [Route("{query=query}")]
        public HttpResponseMessage GetItemUnloged(string query)
        {
            try
            {
                var queryable = _tagService.GetQueryable();

                if (!string.IsNullOrEmpty(query))
                {
                    queryable = queryable.Where(w => w.Name.StartsWith(query));
                }

                var result = queryable.Select(s => s.Name);

                return Request.CreateResponse(HttpStatusCode.Accepted, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
}