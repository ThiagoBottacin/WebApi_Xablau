﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web.Http;
using Xablau.Application.Interfaces;
using Xablau.Domain.Entities;
using Xablau.Domain.Entities.DTO;

namespace Xablau.API.Controllers
{
    [RoutePrefix("api/change")]
    public class ChangeController : ApiController
    {
        #region Services
        private readonly IItemAppService _itemAppService;
        private readonly IItemTagAppService _itemTagService;
        private readonly IChangeAppService _changeAppService;

        public ChangeController(IItemAppService itemAppService, IItemTagAppService itemTagService,
            IChangeAppService changeAppService)
        {
            _itemAppService = itemAppService;
            _itemTagService = itemTagService;
            _changeAppService = changeAppService;
        }
        #endregion


        //Pega combinações dos produtos
        [Authorize(Roles = "User")]
        [HttpGet]
        [Route("matchs")]
        public HttpResponseMessage Matchs()
        {
            try
            {

                int userId = Convert.ToInt32(User.Identity.Name);

                var result = _itemAppService.GetItemsMatchsPerUser(userId);

                return Request.CreateResponse(HttpStatusCode.Accepted, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }


        [Authorize(Roles = "User")]
        [HttpGet]
        [Route("{changeId:int}/details")]
        public HttpResponseMessage Details(int changeId)
        {
            try
            {

                int userId = Convert.ToInt32(User.Identity.Name);

                var result = _changeAppService.GetDetails(changeId, userId);

                return Request.CreateResponse(HttpStatusCode.Accepted, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }


        [Authorize(Roles = "User")]
        [HttpGet]
        [Route("xablau")]
        public HttpResponseMessage AllXablau()
        {
            try
            {
                int userId = Convert.ToInt32(User.Identity.Name);

                var result = _changeAppService.GetAllXablauPerUser(userId);

                return Request.CreateResponse(HttpStatusCode.Accepted, result);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }


        [Authorize(Roles = "User")]
        [HttpPost]
        [Route("like")]
        public HttpResponseMessage Like(Like like)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                try
                {
                    int userId = Convert.ToInt32(User.Identity.Name);
                    bool isXablau = false;

                    if (like.ChangeId != null)
                    {
                        var change = _changeAppService.GetById(like.ChangeId.Value);

                        if (change.Item1.PersonId == userId)
                            change.Like1 = like.Confirmation;
                        else
                            change.Like2 = like.Confirmation;

                        if (!like.Confirmation)
                            change.Status = false;

                        if (change.Like1 == true && change.Like2 == true)
                        {
                            change.Status = true;
                            isXablau = true;
                        }

                        _changeAppService.Update(change);

                    }
                    else
                    {
                        var interestedProduct = _itemAppService.GetQueryable().FirstOrDefault(g => g.ItemId == like.ProductId && g.DataDelete == null);
                        var productTagsWish = _itemTagService.GetQueryable(x => x.ItemId == interestedProduct.ItemId && x.TagType.Name.Contains("desejo"), null, "Item, Tag, TagType").Select(s => s.TagId).ToArray();
                        var userProducts = _itemTagService.GetQueryable(g => g.Item.PersonId == userId && g.Item.DataDelete == null && g.TagType.Name.Contains("descricao") && productTagsWish.Any(a => a == g.TagId), null, "Item, Item.Person, Tag, TagType").GroupBy(p => p.ItemId).Select(g => g.FirstOrDefault()).ToList();

                        if (userProducts.Any() && interestedProduct != null)
                        {
                            foreach (var item in userProducts)
                            {

                                var change = new Change
                                {
                                    //Add info current user
                                    Item1Id = item.ItemId,
                                    Price_1 = item.Item.Price,
                                    Like1 = like.Confirmation,

                                    //Add info interested Product
                                    Item2Id = interestedProduct.ItemId,
                                    Price_2 = interestedProduct.Price,
                                    Like2 = null,

                                    //Others infos
                                    DataSave = DateTime.Now,
                                    Status = null
                                };

                                if (!like.Confirmation)
                                {
                                    change.Status = false;
                                }
                                
                                _changeAppService.Add(change);
                            }
                        }
                    }

                    scope.Complete();
                    return Request.CreateResponse(HttpStatusCode.Accepted, isXablau);
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
        }
    }
}
