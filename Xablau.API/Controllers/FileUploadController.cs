﻿using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Xablau.Application.Utils;

namespace Xablau.API.Controllers
{
    [RoutePrefix("api/upload")]
    public class FileUploadController : ApiController
    {
        [Authorize(Roles = "User")]
        [HttpPost]
        [Route("files")]
        public HttpResponseMessage UploadFile(string type)
        {
            HttpResponseMessage result;
            var httpRequest = HttpContext.Current.Request;

            if (httpRequest.Files.Count > 0)
            {
                var filePath = HttpContext.Current.Server.MapPath("~/Uploads/Temp/" + User.Identity.Name + "/" + type + "/");

                if (!Directory.Exists(filePath))
                    Directory.CreateDirectory(filePath);

                foreach (string file in httpRequest.Files)
                {

                    var postedFile = httpRequest.Files[file];

                    if (postedFile != null)
                    {
                        string fileName = postedFile.FileName;
                        int maxFileNameLength = 40;

                        if (fileName.Length > maxFileNameLength)
                        {
                            int indexFileType = fileName.LastIndexOf('.');

                            int totalRemove = fileName.Length - maxFileNameLength;

                            fileName = fileName.Remove(indexFileType - totalRemove, totalRemove);
                        }

                        var imageResized = ImageHelper.ResizeImage(Image.FromStream(postedFile.InputStream), 250, 250);

                        if (File.Exists(filePath + "\\" + fileName))
                            File.Delete(filePath + "\\" + fileName);

                        imageResized.Save(filePath + fileName);
                    }
                }

                result = Request.CreateResponse(HttpStatusCode.Created);
            }
            else
            {
                result = Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            return result;
        }
    }
}