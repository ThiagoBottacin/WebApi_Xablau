﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web;
using System.Web.Http;
using Xablau.Application.Interfaces;
using Xablau.Domain.Entities;
using Xablau.Domain.Entities.DTO;

namespace Xablau.API.Controllers
{
    [RoutePrefix("api/items")]
    public class ItemsController : ApiController
    {

        #region Services
        private readonly IItemAppService _itemAppService;
        private readonly IItemTagAppService _itemTagService;
        private readonly ITagAppService _tagService;
        private readonly IPictureAppService _pictureAppService;
        private readonly IChangeAppService _changeAppService;


        public ItemsController(IItemAppService itemAppService, IItemTagAppService itemTagService,
            ITagAppService tagService, IPictureAppService pictureAppService,
            IChangeAppService changeAppService)
        {
            _itemAppService = itemAppService;
            _itemTagService = itemTagService;
            _tagService = tagService;
            _pictureAppService = pictureAppService;
            _changeAppService = changeAppService;
        }

        #endregion


        //Insere Itens
        [Authorize(Roles = "User")]
        [HttpPost]
        [Route("")]
        public HttpResponseMessage Insert(ItemAdd item)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                Item newItem = new Item();

                try
                {
                    int userId = Convert.ToInt32(User.Identity.Name);

                    newItem = SaveItem(item, userId);
                    AddTags(newItem, item.WishTag, item.DescriptionTag);
                    AddPhotos(newItem.ItemId, userId);

                    //Match
                    _changeAppService.Match();

                    scope.Complete();
                    return Request.CreateResponse(HttpStatusCode.Accepted);
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    DeleteItemImage(newItem?.ItemId ?? 0, Convert.ToInt32(User.Identity.Name));
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
        }


        //Deletar Item
        [Authorize(Roles = "User")]
        [HttpPut]
        [Route("")]
        public HttpResponseMessage Update(ItemAdd item)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                Item updatedItem = new Item();
                try
                {
                    int userId = Convert.ToInt32(User.Identity.Name);
                    updatedItem = SaveItem(item, userId);

                    //Deleta foto
                    if (item.RemovedFiles != null)
                        DeleteItemImage(updatedItem.ItemId, userId, item.RemovedFiles);

                    //Remove itemTags
                    RemoveTags(updatedItem, item.WishTag, item.DescriptionTag);

                    //Deleta match em aberto
                    DeleteOpenMatchs(updatedItem);

                    AddTags(updatedItem, item.WishTag, item.DescriptionTag);
                    AddPhotos(updatedItem.ItemId, userId);

                    DeleteItemImage(0, userId);
                    scope.Complete();
                    return Request.CreateResponse(HttpStatusCode.Accepted);
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    DeleteItemImage(updatedItem?.ItemId ?? 0, Convert.ToInt32(User.Identity.Name));
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
        }


        //Deletar Item
        [Authorize(Roles = "User")]
        [HttpDelete]
        [Route("")]
        public HttpResponseMessage Delete(int itemId)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                try
                {
                    if (itemId <= 0)
                        throw new Exception("Item Inválido");

                    int userId = Convert.ToInt32(User.Identity.Name);
                    Item item = _itemAppService.Get(g => g.ItemId == itemId);

                    if (item != null && item.PersonId == userId)
                    {

                        item.DataDelete = DateTime.Now.Date;
                        _itemAppService.Update(item);

                        DeleteOpenAndOnGoingMatchs(item);
                    }
                    else
                    {
                        throw new Exception("Operação Inválida");
                    }

                    scope.Complete();
                    return Request.CreateResponse(HttpStatusCode.Accepted);
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
        }

        //Pega detalhes do Item para tela unlogged e logged
        [HttpGet]
        [Route("{itemId=itemId}/details")]
        public HttpResponseMessage Details(int itemId)
        {

            try
            {
                var result = _itemAppService.GetItemDetails(itemId);

                return Request.CreateResponse(HttpStatusCode.Accepted, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }


        //Pega Itens Interessados
        [Authorize(Roles = "User")]
        [HttpGet]
        [Route("interested")]
        public HttpResponseMessage InterestedItems()
        {
            try
            {

                int userId = Convert.ToInt32(User.Identity.Name);

                var result = _itemAppService.GetInterestedItems(userId);

                return Request.CreateResponse(HttpStatusCode.Accepted, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }


        //Retorna as tag do Item
        [HttpGet]
        [Route("{itemId:int}/tags")]
        public IEnumerable<object> ItemTag(int itemId)
        {
            return _itemTagService.GetTagNamesByItem(itemId);
        }


        [HttpGet]
        [Route("search/{name=name}/{locale=locale}/{page=page}/{registers=registers}")]
        public HttpResponseMessage Search(string name, string locale, int page, int registers)
        {

            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(locale))
                return null;

            try
            {
                var result = _itemAppService.SearchItem(name, locale, page, registers);

                return Request.CreateResponse(HttpStatusCode.Accepted, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }


        [HttpGet]
        [Route("{q=query}")]
        public HttpResponseMessage ItemsName(string query)
        {
            try
            {
                var result = _itemAppService.GetItemNames(query);

                return Request.CreateResponse(HttpStatusCode.Accepted, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }


        [HttpGet]
        [Route("unloged/{page=page}/{registers=registers}")]
        public object ItemsUnloged(int page, int registers)
        {
            int totRegisters;
            var items = _itemAppService.GetItemsUnloged(page, registers, out totRegisters);

            var itemResults = items as ItemResult[] ?? items.ToArray();

            var result = new
            {
                items = itemResults,
                totRegisters
            };

            return result;
        }


        //Pega todos os Itens por Usuário
        [Authorize(Roles = "User")]
        [HttpGet]
        [Route("my-items/{page=page}/{registers=registers}")]
        public object ItemsByUser(int page, int registers)
        {
            try
            {
                int totRegisters;
                var itemsByUser = _itemAppService.GetAllItemsByUser(Convert.ToInt32(User.Identity.Name), page, registers, out totRegisters);

                var items = itemsByUser as ItemResult[] ?? itemsByUser.ToArray();

                var response = new
                {
                    items,
                    totRegisters
                };

                return Request.CreateResponse(HttpStatusCode.Accepted, response);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }


        #region Métodos Privados

        private Item SaveItem(ItemAdd item, int userId)
        {

            if (item?.Name == null || item.Description == null || item.DescriptionTag == null || item.WishTag == null)
                throw new Exception("Item Inválido");

            Item _item;

            if (item.Id.HasValue && item.Id.Value > 0)
            {

                _item = _itemAppService.GetItemById(item.Id.Value);

                if (_item != null && _item.PersonId == userId)
                {

                    _item.Name = item.Name;
                    _item.Price = Convert.ToDecimal(item.Price);
                    _item.Description = item.Description;

                    _itemAppService.Update(_item);
                }
                else
                {
                    throw new Exception("Item inválido");
                }
            }
            else
            {

                _item = new Item
                {
                    Name = item.Name,
                    Price = Convert.ToDecimal(item.Price),
                    DataDelete = null,
                    Description = item.Description,
                    DataSave = DateTime.Now,
                    PersonId = userId
                };


                //Valida se já tem Item igual
                if (_itemAppService.ValidateItemPerUser(_item))
                    throw new Exception("Já possui Item cadastrado!");

                //Insere Item
                _itemAppService.Add(_item);
            }

            return _item;
        }

        #region Matchs

        private void DeleteOpenMatchs(Item item)
        {
            _changeAppService.DeleteOpenMatchsByItemId(item.ItemId);
        }

        private void DeleteOpenAndOnGoingMatchs(Item item)
        {
            _changeAppService.DeleteOpenAndOnGoingMatchs(item.ItemId);
        }
        #endregion

        #region Tags
        private void AddTags(Item item, string[] wishTag, string[] descriptionTag)
        {

            //Junta todas as tags para fazer o insert na tabela Tag
            var tags = wishTag.Concat(descriptionTag);

            //Insere Tags
            _tagService.InsertAll(tags);

            //Insere Item_Tag 
            _itemTagService.InsertAllItemTag(item, wishTag, "desejo");
            _itemTagService.InsertAllItemTag(item, descriptionTag, "descricao");
        }

        private void RemoveTags(Item item, string[] wishTag, string[] descriptionTag)
        {
            List<ItemTag> tagsToDelete = new List<ItemTag>();
            var currentTags = _itemTagService.GetAllTagsByItem(item.ItemId).ToList();

            tagsToDelete.AddRange(currentTags.Where(w => wishTag.Any(a => a != w.Tag.Name && w.TagTypeId == 2)));
            tagsToDelete.AddRange(currentTags.Where(w => descriptionTag.Any(a => a != w.Tag.Name && w.TagTypeId == 1)));

            _itemTagService.DeleteMany(tagsToDelete);
        }
        #endregion

        #region Photos

        //Salva as fotos do item no diretório padrão
        private string[] SaveItemImageServer(int itemId, int userId)
        {
            string type = "Items";
            var defaultPath = HttpContext.Current.Server.MapPath("~/Uploads" + "/" + type + "/" + userId + "/" + itemId + "/");
            var tempPath = HttpContext.Current.Server.MapPath("~/Uploads/Temp/" + userId + "/" + type + "/");
            var userPath = HttpContext.Current.Server.MapPath("~/Uploads" + "/" + type + "/" + userId + "/");

            if (!Directory.Exists(tempPath))
                return null;

            if (!Directory.Exists(defaultPath))
                Directory.CreateDirectory(defaultPath);

            var files = Directory.GetFiles(tempPath);

            foreach (var file in files)
            {
                if (!File.Exists(defaultPath + new FileInfo(file).Name))
                {
                    File.Move(tempPath + new FileInfo(file).Name, defaultPath + new FileInfo(file).Name);
                }
            }

            return FormatUrl(Directory.GetFiles(defaultPath));
        }

        private void AddPhotos(int itemId, int userId)
        {

            var urlsPictures = SaveItemImageServer(itemId, userId);

            if (urlsPictures != null && urlsPictures.Any())
                _pictureAppService.InsertAllPicturesPerItem(urlsPictures, itemId);
        }

        private void DeleteItemImage(int itemId, int userId)
        {
            string type = "Items";
            string srcPath = string.Empty;

            if (itemId > 0)
            {
                srcPath = HttpContext.Current.Server.MapPath($"~/Uploads/{type}/{userId}/{itemId}/");
                _pictureAppService.DeleteByItem(itemId);
            }
                
            else
                srcPath = HttpContext.Current.Server.MapPath($"~/Uploads/Temp/{userId}/{type}/");

            if (Directory.Exists(srcPath))
                Directory.Delete(srcPath, true);
        }

        private void DeleteItemImage(int itemId, int userId, string[] removedFiles)
        {
            string type = "Items";
            string srcPath = string.Empty;

            if (removedFiles != null)
            {
                foreach (var file in removedFiles)
                {
                    srcPath = HttpContext.Current.Server.MapPath($"~/Uploads/{type}/{userId}/{itemId}/{file}");

                    _pictureAppService.DeleteByNameAndItemId(file, itemId);

                    if (File.Exists(srcPath))
                        File.Delete(srcPath);
                }
            }
        }

        //Formata as urls das fotos e retorna as urls tratadas
        private string[] FormatUrl(string[] urls)
        {
            if (urls.Any())
            {
                string[] urlsResult = new string[urls.Count()];

                int cont = 0;
                foreach (var item in urls)
                {
                    var urlReplaced = item.Replace("\\", "/");
                    var urlSplited = urlReplaced.Split('/');

                    var urlBase = Request.RequestUri;
                    string urlComplement = "";

                    string image;

                    bool concatena = false;
                    for (int i = 0; i < urlSplited.Length; i++)
                    {
                        if (urlSplited[i] == "Uploads" || concatena)
                        {
                            concatena = true;
                            urlComplement += i != urlSplited.Length - 1 ? urlSplited[i] + "/" : urlSplited[i];
                        }
                    }

                    if (urlBase.ToString().Contains("http://localhost:"))
                        image = ConfigurationManager.AppSettings["UrlApiLocal"] + urlComplement;
                    else
                        image = ConfigurationManager.AppSettings["UrlApiAzure"] + urlComplement;

                    urlsResult[cont] = image;
                    cont++;
                }

                return urlsResult;
            }

            return null;
        }
        #endregion

        #endregion
    }
}