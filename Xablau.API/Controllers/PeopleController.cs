﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Xablau.Application.Interfaces;
using Xablau.Domain.Entities;
using Xablau.Domain.Entities.DTO;

namespace Xablau.API.Controllers
{
    [RoutePrefix("api/user")]
    public class PeopleController : ApiController
    {

        #region Services

        private readonly IPersonAppService _personAppService;
        private readonly IPhoneAppService _phoneAppService;

        public PeopleController(IPersonAppService personAppService, IPhoneAppService phoneAppService)
        {
            _personAppService = personAppService;
            _phoneAppService = phoneAppService;
        }

        #endregion


        [HttpPost]
        [Route("register")]
        public HttpResponseMessage Register(Person person)
        {

            var user = _personAppService.Get(p => p.Email == person.Email);

            if (user != null)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Email já existe, tente outro!");

            try
            {
                _personAppService.Add(person);
                return Request.CreateResponse(HttpStatusCode.Accepted, "Sucess");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }

        }


        [Authorize(Roles = "User")]
        [HttpPut]
        [Route("edit")]
        public HttpResponseMessage Edit(Person person)
        {

            int userId = int.Parse(User.Identity.Name);

            var user = _personAppService.Get(p => p.PersonId == userId);

            bool alter = false;

            if (person.Email != user.Email)
            {
                var email = _personAppService.Get(p => p.Email == person.Email);
                if (email == null)
                {
                    user.Email = person.Email;
                    alter = true;
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Email já existe, tente outro!");
                }
            }

            if (person.Name != user.Name)
            {
                user.Name = person.Name;
                alter = true;
            }


            if (alter)
            {
                try
                {
                    _personAppService.Update(user);
                    return Request.CreateResponse(HttpStatusCode.Accepted, "Sucess");
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }

            return Request.CreateResponse(HttpStatusCode.Forbidden, "Forbidden"); ;
        }


        [Authorize(Roles = "User")]
        [HttpGet]
        [Route("validate")]
        public object Validate()
        {
            var user = _personAppService.Get(x => x.PersonId == int.Parse(User.Identity.Name));

            var userphone = _phoneAppService.GetAll(x => x.PersonId == int.Parse(User.Identity.Name)).FirstOrDefault();

            if (userphone == null)
            {
                return new

                {
                    name = user.Name,
                    email = user.Email,
                    dd = "",
                    phone = ""
                };
            }

            return new

            {
                name = user.Name,
                email = user.Email,
                dd = userphone.DDD,
                phone = userphone.Number

            };
        }


        [Authorize(Roles = "User")]
        [HttpPost]
        [Route("change-password")]
        public HttpResponseMessage ChangePassword(ChangePassword password)
        {

            var user = _personAppService.Get(p => p.PersonId == int.Parse(User.Identity.Name));

            try
            {
                if (user.Password == password.OldPassword)
                {
                    user.Password = password.NewPassword;
                    _personAppService.Update(user);
                    return Request.CreateResponse(HttpStatusCode.Accepted, "Sucesss");
                }

                return Request.CreateResponse(HttpStatusCode.Accepted, "Invalid");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
}