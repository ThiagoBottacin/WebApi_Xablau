﻿using Microsoft.Owin.Security.OAuth;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using Xablau.Application.Interfaces;
using Xablau.Domain.Entities;

namespace Xablau.API
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        private readonly IPersonAppService _personAppService;

        public AuthorizationServerProvider(IPersonAppService personAppService)
        {
            _personAppService = personAppService;
        }

        //private readonly IPersonAppService _personAppService;

        //public AuthorizationServerProvider(IPersonAppService personAppService)
        //{
        //    _personAppService = personAppService;
        //}

        //Valida o token no cache, que o OAuth é responsavel. Caso o token nao dor válido
        //vai para o proximo metodo
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        //Responsavel pela autenticacao.
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            //A requisicao vem de outra origem, por isso permitimos os acessos
            //context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            try
            {
                context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

                //Receemos dois context para autenticacao
                var user = context.UserName;
                var password = context.Password;

                Person person = _personAppService.Get(x => x.Email == user);

                if (person == null || person.Password != password)
                {
                    context.SetError("invalid_grant", "Usuário ou Senha Inválidos.");
                    return;
                }

                var identity = new ClaimsIdentity(context.Options.AuthenticationType);

                identity.AddClaim(new Claim(ClaimTypes.Name, person.PersonId.ToString()));
                //identity.AddClaim(new Claim(ClaimTypes.Name, person.Name));
                //identity.AddClaim(new Claim(ClaimTypes.Email, person.Email));
                //identity.AddClaim(new Claim(ClaimTypes.Sid, person.PersonId.ToString()));

                var roles = new List<string>();
                //roles.Add("Admin");
                roles.Add("User");

                foreach (var role in roles)
                    identity.AddClaim(new Claim(ClaimTypes.Role, role));

                GenericPrincipal principal = new GenericPrincipal(identity, roles.ToArray());
                Thread.CurrentPrincipal = principal;

                context.Validated(identity);
            }
            catch
            {
                context.SetError("invalid_grant", "Falha ao autenticar");
            }
        }
    }
}