﻿using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Web.Http;
using Ninject;
using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi.OwinHost;
using System.Reflection;
using Xablau.Application.Interfaces;
using Xablau.Application.Services;
using Xablau.Domain.Repositories;
using Xablau.Infra.Repositories;

namespace Xablau.API
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();

            ConfigureWebApi(config);
            ConfigureOAuth(app);

            //Permite requisicoes de qualquer URL
            app.UseCors(CorsOptions.AllowAll);

            app.UseWebApi(config);
            app.UseNinjectMiddleware(CreateKernel).UseNinjectWebApi(config);

        }

        public static void ConfigureWebApi(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
                );

        }

        

        public void ConfigureOAuth(IAppBuilder app)
        {
            OAuthAuthorizationServerOptions oAuthServerOptions = new OAuthAuthorizationServerOptions
            {
                //Requisicoes não segurar, acessar sem https
                AllowInsecureHttp = true,
                //Para onde vou fazer a requisicao para obter o token
                TokenEndpointPath = new PathString("/api/security/token"),
                //Tempo para acesso expirar
                AccessTokenExpireTimeSpan = TimeSpan.FromHours(2),
                //Quem que vai autenticar o servico
                //Poderia add o provider do facebook aqui
                Provider = new AuthorizationServerProvider(new PersonAppService(new PersonRepository()))
            };

            app.UseOAuthAuthorizationServer(oAuthServerOptions);
            //Autenticacao via Bearer
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }


        private static StandardKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            RegisterServices(kernel);
            return kernel;
        }

        private static void RegisterServices(StandardKernel kernel)
        {
            //Ninject Person
            kernel.Bind(typeof(IAppServiceBase<>)).To(typeof(AppServiceBase<>));
            kernel.Bind<IPersonAppService>().To<PersonAppService>();

            //kernel.Bind(typeof(IAppServiceBase<>)).To(typeof(AppServiceBase<>));
            //kernel.Bind<IPersonAppService>().To<PersonAppService>();

            kernel.Bind(typeof(IRepositoryBase<>)).To(typeof(RepositoryBase<>));
            kernel.Bind<IPersonRepository>().To<PersonRepository>();

            //kernel.Bind<IPersonAppService>().To<PersonAppService>();

            //Ningect Change
            kernel.Bind<IChangeAppService>().To<ChangeAppService>();
            kernel.Bind<IChangeRepository>().To<ChangeRepository>();

            //Ninject Item
            kernel.Bind<IItemAppService>().To<ItemAppService>();
            kernel.Bind<IItemRepository>().To<ItemRepository>();

            //Ningect ItemTag
            kernel.Bind<IItemTagAppService>().To<ItemTagAppService>();
            kernel.Bind<IItemTagRepository>().To<ItemTagRepository>();

            //Ningect Message
            kernel.Bind<IMessageAppService>().To<MessageAppService>();
            kernel.Bind<IMessageRepository>().To<MessageRepository>();

            //Ningect Phone
            kernel.Bind<IPhoneAppService>().To<PhoneAppService>();
            kernel.Bind<IPhoneRepository>().To<PhoneRepository>();

            //Ningect Tag
            kernel.Bind<ITagAppService>().To<TagAppService>();
            kernel.Bind<ITagRepository>().To<TagRepository>();

            //Ningect TypeTag
            kernel.Bind<ITagTypeAppService>().To<TagTypeAppService>();
            kernel.Bind<ITagTypeRepository>().To<TagTypeRepository>();
            
            //Ningect Address
            kernel.Bind<IAddressAppService>().To<AddressAppService>();
            kernel.Bind<IAddressRepository>().To<AddressRepository>();
           
            //Ningect Picture
            kernel.Bind<IPictureAppService>().To<PictureAppService>();
            kernel.Bind<IPictureRepository>().To<PictureRepository>();
        }
    }
}