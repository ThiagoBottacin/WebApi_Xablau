﻿using Xablau.Domain.Entities;

namespace Xablau.Application.Interfaces
{
    public interface IPictureAppService : IAppServiceBase<Picture>
    {
        void InsertAllPicturesPerItem(string[] urlPictures, int productId);

        void DeleteByNameAndItemId(string fileName, int itemId);

        void DeleteByItem(int itemId);
    }
}
