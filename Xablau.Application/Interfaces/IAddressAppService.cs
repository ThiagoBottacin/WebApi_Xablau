﻿using System.Collections.Generic;
using Xablau.Domain.Entities;

namespace Xablau.Application.Interfaces
{
    public interface IAddressAppService : IAppServiceBase<Address>
    {
        IEnumerable<string> GetNamesLocality(string query);

        Address Get(int addressId, int userId);
    }
}
