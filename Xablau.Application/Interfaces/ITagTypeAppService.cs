﻿using Xablau.Domain.Entities;

namespace Xablau.Application.Interfaces
{
    public interface ITagTypeAppService : IAppServiceBase<TagType>
    {
    }
}
