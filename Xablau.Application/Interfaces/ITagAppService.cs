﻿using System.Collections.Generic;
using Xablau.Domain.Entities;

namespace Xablau.Application.Interfaces
{
    public interface ITagAppService : IAppServiceBase<Tag>
    {
        void InsertAll(IEnumerable<string> tags);
    }
}
