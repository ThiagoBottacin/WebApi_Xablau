﻿using System.Collections.Generic;
using System.Linq;
using Xablau.Domain.Entities;
using Xablau.Domain.Entities.DTO;

namespace Xablau.Application.Interfaces
{
    public interface IItemAppService : IAppServiceBase<Item>
    {
        Item GetItemById(int itemId);

        IEnumerable<ItemResult> GetAllItemsByUser(int idUsuario, int page, int registers, out int totRegisters);

        IQueryable<Item> GetQueryable();

        IEnumerable<string> GetItemNames(string query);

        IEnumerable<ItemResult> GetItemsUnloged(int page, int registers, out int totRegisters);

        bool ValidateItemPerUser(Item item);

        IEnumerable<ItemResult> SearchItem(string name, string locale, int page, int registers);

        IEnumerable<ItemResult> GetItemsMatchsPerUser(int userId);

        IEnumerable<ItemResult> GetItemDetails(int itemId);

        IEnumerable<ItemResult> GetInterestedItems(int userId);
    }
}
