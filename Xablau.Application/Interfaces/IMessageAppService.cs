﻿using Xablau.Domain.Entities;

namespace Xablau.Application.Interfaces
{
    public interface IMessageAppService : IAppServiceBase<Message>
    {
    }
}
