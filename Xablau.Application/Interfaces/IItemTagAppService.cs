﻿using System.Collections.Generic;
using Xablau.Domain.Entities;

namespace Xablau.Application.Interfaces
{
    public interface IItemTagAppService : IAppServiceBase<ItemTag>
    {
        IEnumerable<object> GetTagNamesByItem(int itemId);

        void InsertAllItemTag(Item item, string[] listTags, string tagType);

        void DeleteByItemId(int itemId);


        void DeleteMany(IEnumerable<ItemTag> itemTags);

        IEnumerable<ItemTag> GetAllTagsByItem(int itemId);
    }
}
