﻿using Xablau.Domain.Entities;

namespace Xablau.Application.Interfaces
{
    public interface IPersonAppService : IAppServiceBase<Person>
    {
    }
}
