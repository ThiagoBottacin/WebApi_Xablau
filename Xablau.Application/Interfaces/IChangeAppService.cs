﻿using System.Collections.Generic;
using Xablau.Domain.Entities;
using Xablau.Domain.Entities.DTO;

namespace Xablau.Application.Interfaces
{
    public interface IChangeAppService : IAppServiceBase<Change>
    {

        IEnumerable<Change> GetMatchsPerUser(int userId);

        IEnumerable<ItemResult> GetAllXablauPerUser(int userId);

        object GetDetails(int changeId, int userId);

        void Match();

        void DeleteOpenMatchsByItemId(int itemId);

        void DeleteOpenAndOnGoingMatchs(int itemId);
    }
}