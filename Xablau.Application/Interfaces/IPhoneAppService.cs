﻿using Xablau.Domain.Entities;

namespace Xablau.Application.Interfaces
{
    public interface IPhoneAppService : IAppServiceBase<Phone>
    {
    }
}
