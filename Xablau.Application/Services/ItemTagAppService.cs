﻿using System.Collections.Generic;
using System.Linq;
using Xablau.Application.Interfaces;
using Xablau.Domain.Entities;
using Xablau.Domain.Repositories;

namespace Xablau.Application.Services
{
    public class ItemTagAppService : AppServiceBase<ItemTag>, IItemTagAppService
    {
        private readonly IItemTagRepository _itemtagRepository;
        private readonly ITagAppService _tagService;
        private readonly ITagTypeAppService _tagTypeService;

        public ItemTagAppService(IItemTagRepository itemtagRepository, ITagAppService tagService, ITagTypeAppService tagTypeService) 
            : base(itemtagRepository)
        {
            _itemtagRepository = itemtagRepository;
            _tagService = tagService;
            _tagTypeService = tagTypeService;
        }

        public IEnumerable<object> GetTagNamesByItem(int itemId)
        {
            var tags = _itemtagRepository.GetAll(x => x.ItemId == itemId && x.TagTypeId == 2).Select(s => new 
            {
                name = s.Tag.Name
            });

            return tags;
        }

        public IEnumerable<ItemTag> GetAllTagsByItem(int itemId)
        {
            var itemTags = _itemtagRepository.GetQueryable(x => x.ItemId == itemId, null, "Tag");

            return itemTags;
        }

        public void InsertAllItemTag(Item item, string[] listTags, string tagType)
        {
            TagType tgType = _tagTypeService.Get(x => x.Name.Contains(tagType));

            foreach (var tag in listTags)
            {

                var _tag = _tagService.Get(x => x.Name.ToLower().Equals(tag.ToLower()));

                var hasItemTag = _itemtagRepository.GetQueryable(q =>
                    q.ItemId == item.ItemId && q.TagId == _tag.TagId && q.TagTypeId == tgType.TagTypeId).FirstOrDefault() != null;

                if (!hasItemTag)
                {
                    ItemTag itemTag = new ItemTag
                    {
                        ItemId = item?.ItemId ?? 0,
                        TagId = _tag?.TagId ?? 0,
                        TagTypeId = tgType?.TagTypeId ?? 0
                    };

                    if (itemTag.ItemId != 0 && itemTag.TagId != 0 && itemTag.TagTypeId != 0)
                    {
                        _itemtagRepository.Add(itemTag);
                    }
                }
            }
        }

        public void DeleteByItemId(int itemId)
        {
            IEnumerable <ItemTag> itemTags = _itemtagRepository.GetQueryable(g => g.ItemId == itemId).ToList();

            foreach (var itemTag in itemTags)
            {
                _itemtagRepository.Remove(itemTag);
            }
        }

        public void DeleteMany(IEnumerable<ItemTag> itemTags)
        {
            foreach (var itemTag in itemTags)
            {
                _itemtagRepository.Remove(itemTag);
            }
        }
    }
}