﻿using System;
using System.Linq;
using Xablau.Application.Interfaces;
using Xablau.Domain.Entities;
using Xablau.Domain.Repositories;

namespace Xablau.Application.Services
{
    public class PictureAppService : AppServiceBase<Picture>, IPictureAppService
    {
        private readonly IPictureRepository _pictureRepository;

        public PictureAppService(IPictureRepository pictureRepository) 
            : base(pictureRepository)
        {
            _pictureRepository = pictureRepository;
        }

        public void InsertAllPicturesPerItem(string[] urlPictures, int itemId)
        {
            if (!urlPictures.Any() || itemId == 0)
                return;

            foreach (var photo in urlPictures)
            {
                var hasPhoto = GetQueryable(g => g.Image.Contains(photo) && g.ItemId == itemId && g.DataDelete == null).FirstOrDefault() != null;

                if (!hasPhoto)
                {
                    Picture picture = new Picture()
                    {
                        Image = photo,
                        DataSave = DateTime.Now.Date,
                        ItemId = itemId
                    };

                    _pictureRepository.Add(picture);
                }
            }
        }

        public void DeleteByNameAndItemId(string fileName, int itemId)
        {
            var photo = _pictureRepository.GetQueryable(g => g.Image.Contains(fileName) && g.ItemId == itemId && g.DataDelete == null).FirstOrDefault();

            if (photo != null)
            {
                photo.DataDelete = DateTime.Now;
                _pictureRepository.Update(photo);
            }
        }

        public void DeleteByItem(int itemId)
        {
            var photos = _pictureRepository.GetQueryable(g => g.ItemId == itemId);

            foreach (var photo in photos)
            {
                photo.DataDelete = DateTime.Now;
                _pictureRepository.Update(photo);
            }
        }
    }
}
