﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xablau.Application.Interfaces;
using Xablau.Domain.Entities;
using Xablau.Domain.Entities.DTO;
using Xablau.Domain.Repositories;

namespace Xablau.Application.Services
{
    public class ChangeAppService : AppServiceBase<Change>, IChangeAppService
    {
        private readonly IChangeRepository _changeRepository;
        private readonly IPictureAppService _pictureAppService;
        private readonly IAddressAppService _addressAppService;
        private readonly IItemTagAppService _itemTagService;

        public ChangeAppService(IChangeRepository changeRepository, IPictureAppService pictureAppService,
            IAddressAppService addressAppService, IItemTagAppService itemTagAppService)
            : base(changeRepository)
        {
            _changeRepository = changeRepository;
            _pictureAppService = pictureAppService;
            _addressAppService = addressAppService;
            _itemTagService = itemTagAppService;
        }

        public IEnumerable<Change> GetMatchsPerUser(int userId)
        {
            return _changeRepository.GetQueryable(x =>
                (x.Item1.PersonId == userId && x.Like1 == null || x.Item2.PersonId == userId && x.Like2 == null) &&
                x.Like1 != false && x.Like2 != false &&
                x.Status == null && x.DataDelete == null).OrderByDescending(o => o.DataSave);
        }

        public IEnumerable<ItemResult> GetAllXablauPerUser(int userId)
        {
            return _changeRepository
                .GetQueryable(x =>
                    (x.Item1.PersonId == userId || x.Item2.PersonId == userId) && (x.Like1 == true && x.Like2 == true))
                .OrderByDescending(o => o.DataSave).ToList().Select(s => new ItemResult
                {
                    ChangeId = s.ChangeId,
                    ItemId = s.Item1.PersonId != userId ? s.Item1Id : s.Item2Id,
                    Name = s.Item1.PersonId != userId ? s.Item1.Name : s.Item2.Name,
                    Description = s.Item1.PersonId != userId ? s.Item1.Description : s.Item2.Description,
                    Price = s.Item1.PersonId != userId ? s.Item1.Price : s.Item2.Price,
                    DataSave = s.Item1.PersonId != userId ? s.Item1.DataSave : s.Item2.DataSave,
                    UrlPhotos =
                        _pictureAppService.GetAll(g =>
                            g.ItemId == (s.Item1.PersonId != userId ? s.Item1Id : s.Item2Id) && g.DataDelete == null) !=
                        null
                            ? _pictureAppService
                                .GetAll(g =>
                                    g.ItemId == (s.Item1.PersonId != userId ? s.Item1Id : s.Item2Id) &&
                                    g.DataDelete == null).Select(ss => ss.Image).ToArray()
                            : null
                });
        }

        public object GetDetails(int changeId, int userId)
        {
            var result = _changeRepository
                .GetQueryable(c =>
                    c.DataDelete == null && (c.Item1.PersonId == userId || c.Item2.PersonId == userId) && c.ChangeId == changeId)
                .ToList().Select(s => new
                {
                    Name = s.Item1.PersonId != userId ? s.Item1.Name : s.Item2.Name,
                    Description = s.Item1.PersonId != userId ? s.Item1.Description : s.Item2.Description,
                    Price = s.Item1.PersonId != userId ? s.Item1.Price : s.Item2.Price,
                    Address =
                    _addressAppService.Get(g => g.PersonId == (s.Item1.PersonId != userId ? s.Item1.PersonId : s.Item2.PersonId)) !=
                    null
                        ? _addressAppService.Get(g => g.PersonId == (s.Item1.PersonId != userId ? s.Item1.PersonId : s.Item2.PersonId))
                            .State.ToString()
                        : null,
                    WishTag = _itemTagService
                        .GetAll(g =>
                            g.ItemId == (s.Item1.PersonId != userId ? s.Item1Id : s.Item2Id) &&
                            g.TagType.Name.Contains("desejo")).Select(ss => ss.Tag.Name).ToArray(),
                    UrlPhotos =
                    _pictureAppService.GetAll(g =>
                        g.ItemId == (s.Item1.PersonId != userId ? s.Item1Id : s.Item2Id) && g.DataDelete == null) != null
                        ? _pictureAppService
                            .GetAll(g =>
                                g.ItemId == (s.Item1.PersonId != userId ? s.Item1Id : s.Item2Id) && g.DataDelete == null)
                            .Select(ss => ss.Image).ToArray()
                        : null
                }).FirstOrDefault();

            return result;
        }

        public void Match()
        {
            _changeRepository.Match();
        }

        public void DeleteOpenMatchsByItemId(int itemId)
        {
            var matchs = _changeRepository.GetQueryable(g => g.Like1 == null && g.Like2 == null 
                && g.Status == null && g.DataDelete == null && (g.Item1Id == itemId || g.Item2Id == itemId)).ToList();

            foreach (var match in matchs)
            {
                match.DataDelete = DateTime.Now;
                _changeRepository.Update(match);
            }
        }

        public void DeleteOpenAndOnGoingMatchs(int itemId)
        {
            var matchs = _changeRepository.GetQueryable(g => g.Status == null && g.DataDelete == null 
                && (g.Item1Id == itemId || g.Item2Id == itemId));

            foreach (var match in matchs)
            {
                match.DataDelete = DateTime.Now;
                _changeRepository.Update(match);
            }
        }
    }
}
