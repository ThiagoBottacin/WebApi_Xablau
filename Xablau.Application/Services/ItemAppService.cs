﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Web;
using Xablau.Application.Interfaces;
using Xablau.Domain.Entities;
using Xablau.Domain.Entities.DTO;
using Xablau.Domain.Repositories;

namespace Xablau.Application.Services
{
    public class ItemAppService : AppServiceBase<Item>, IItemAppService
    {
        private readonly IItemRepository _itemRepository;
        private readonly IPictureAppService _pictureAppService;
        private readonly IAddressAppService _addressAppService;
        private readonly IChangeAppService _changeAppService;
        private readonly IItemTagAppService _itemTagService;

        public ItemAppService(IItemRepository itemRepository,
            IPictureAppService pictureAppService, IAddressAppService addressAppService, IChangeAppService changeAppService, IItemTagAppService itemTagService)
            : base(itemRepository)
        {
            _itemRepository = itemRepository;
            _pictureAppService = pictureAppService;
            _addressAppService = addressAppService;
            _changeAppService = changeAppService;
            _itemTagService = itemTagService;
        }

        public Item GetItemById(int itemId)
        {
            return _itemRepository.GetQueryable().FirstOrDefault(g => g.ItemId == itemId && g.DataDelete == null);
        }


        public IEnumerable<ItemResult> GetAllItemsByUser(int idUsuario, int page, int registers, out int totRegisters)
        {
            var skip = (page - 1) * registers;

            var queryable = _itemRepository.GetQueryable(w => w.PersonId == idUsuario && w.DataDelete == null)
                .OrderByDescending(o => o.DataSave);

            totRegisters = queryable.Count();

            return queryable.Skip(skip).Take(registers).ToList().Select(s => new ItemResult
            {
                ItemId = s.ItemId,
                Name = s.Name,
                Description = s.Description,
                Price = s.Price,
                DataSave = s.DataSave.Date,
                UrlPhotos = _pictureAppService.GetAll(g => g.ItemId == s.ItemId && g.DataDelete == null) != null ? _pictureAppService.GetAll(g => g.ItemId == s.ItemId && g.DataDelete == null).Select(ss => ss.Image).ToArray() : null
            });
        }


        public IEnumerable<ItemResult> GetItemsMatchsPerUser(int userId)
        {
            return _changeAppService.GetMatchsPerUser(userId).ToList()
                .Select(s => new ItemResult
                {
                    ChangeId = s.ChangeId,
                    ItemId = s.Item1.PersonId != userId ? s.Item1Id : s.Item2Id,
                    Name = s.Item1.PersonId != userId ? s.Item1.Name : s.Item2.Name,
                    Description = s.Item1.PersonId != userId ? s.Item1.Description : s.Item2.Description,
                    Price = s.Item1.PersonId != userId ? s.Item1.Price : s.Item2.Price,
                    UrlPhotos = _pictureAppService.GetAll(g => g.ItemId == (s.Item1.PersonId != userId ? s.Item1Id : s.Item2Id) && g.DataDelete == null) != null ? _pictureAppService.GetAll(g => g.ItemId == (s.Item1.PersonId != userId ? s.Item1Id : s.Item2Id) && g.DataDelete == null).Select(ss => ss.Image).ToArray() : null
                });
        }

        public IQueryable<Item> GetQueryable()
        {
            return _itemRepository.GetQueryable();
        }

        public IEnumerable<string> GetItemNames(string query)
        {

            var queryable = _itemRepository.GetQueryable();

            if (!string.IsNullOrEmpty(query))
            {
                queryable = queryable.Where(w => w.Name.StartsWith(query));
            }

            return queryable.Select(s => s.Name);
        }

        public IEnumerable<ItemResult> GetItemsUnloged(int page, int registers, out int totRegisters)
        {
            var skip = (page - 1) * registers;

            var queryable = _itemRepository.GetQueryable(g => g.DataDelete == null).OrderByDescending(o => o.DataSave);
            totRegisters = queryable.Count();

            var result = queryable.Skip(skip).Take(registers).ToList().Select(s => new ItemResult
            {
                ItemId = s.ItemId,
                Name = s.Name,
                Description = s.Description,
                Price = s.Price,
                UrlPhotos = _pictureAppService.GetAll(g => g.ItemId == s.ItemId) != null ? _pictureAppService.GetAll(g => g.ItemId == s.ItemId).Select(ss => ss.Image).ToArray() : null,
                Address = _addressAppService.Get(g => g.PersonId == s.PersonId) != null ? _addressAppService.Get(g => g.PersonId == s.PersonId).State : null
            });

            return result;
        }

        public bool ValidateItemPerUser(Item item)
        {
            return _itemRepository.GetQueryable(g => g.Name == item.Name && g.PersonId == item.PersonId && g.DataDelete == null).FirstOrDefault() != null;
        }

        public IEnumerable<ItemResult> SearchItem(string name, string locale, int page, int registers)
        {

            var skip = (page - 1) * registers;

            var queryable = _itemRepository.GetQueryable(x => x.DataDelete == null && x.Name.Contains(name))
                    .OrderByDescending(o => o.DataSave);

            var result = queryable.Skip(skip).Take(registers).ToList()
                .Select(s => new ItemResult
                {
                    ItemId = s.ItemId,
                    Name = s.Name,
                    Description = s.Description,
                    Price = s.Price,
                    UrlPhotos = _pictureAppService.GetAll(g => g.ItemId == s.ItemId && g.DataDelete == null) != null ? _pictureAppService.GetAll(g => g.ItemId == s.ItemId && g.DataDelete == null).Select(ss => ss.Image).ToArray() : null
                });

            return result;
        }


        public IEnumerable<ItemResult> GetItemDetails(int itemId)
        {
            return _itemRepository.GetQueryable(g => g.ItemId == itemId && g.DataDelete == null, null, "Person").ToList()
                 .Select(s => new ItemResult
                 {
                     ItemId = s.ItemId,
                     Name = s.Name,
                     Description = s.Description,

                     Address = _addressAppService.Get(g => g.PersonId == s.Person.PersonId) != null
                        ? _addressAppService.Get(g => g.PersonId == s.Person.PersonId).State.ToString() : null,

                     Price = s.Price,
                     DescriptionTag = _itemTagService.GetQueryable(g => g.ItemId == s.ItemId && g.TagType.Name.Contains("descricao")).Select(ss => ss.Tag.Name).ToArray(),
                     WishTag = _itemTagService.GetQueryable(g => g.ItemId == s.ItemId && g.TagType.Name.Contains("desejo")).Select(ss => ss.Tag.Name).ToArray(),

                     Photos = _pictureAppService.GetAll(g => g.ItemId == s.ItemId && g.DataDelete == null) != null ? _pictureAppService.GetAll(g => g.ItemId == s.ItemId && g.DataDelete == null).Select(ss => new Photo
                     {
                         Name = Path.GetFileName($"{HttpRuntime.AppDomainAppPath}{new Uri(ss.Image).AbsolutePath}"),
                         Size =  new FileInfo($"{HttpRuntime.AppDomainAppPath}{new Uri(ss.Image).AbsolutePath}").Length / 1024,
                         UrlPhoto = ss.Image
                     }).ToList() : null,

                     UrlPhotos = _pictureAppService.GetAll(g => g.ItemId == s.ItemId && g.DataDelete == null) != null ? _pictureAppService.GetAll(g => g.ItemId == s.ItemId && g.DataDelete == null)
                         .Select(ss => ss.Image).ToArray() : null

                 }).ToList();
        }

        public IEnumerable<ItemResult> GetInterestedItems(int userId)
        {
            var items = _itemRepository.GetInterestedItems(userId)?.Select(s => new ItemResult
            {
                ItemId = s.ItemId,
                Name = s.Name,
                Price = s.Price,
                UrlPhotos = _pictureAppService.GetAll(g => g.ItemId == s.ItemId && g.DataDelete == null) != null
                    ? _pictureAppService.GetAll(g => g.ItemId == s.ItemId && g.DataDelete == null)
                        .Select(ss => ss.Image).ToArray() : null
            });

            return items;
        }
    }
}
