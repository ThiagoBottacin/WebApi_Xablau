﻿using System.Collections.Generic;
using System.Linq;
using Xablau.Application.Interfaces;
using Xablau.Domain.Entities;
using Xablau.Domain.Repositories;

namespace Xablau.Application.Services
{
    public class AddressAppService : AppServiceBase<Address>, IAddressAppService
    {

        private readonly IAddressRepository _addressRepository;

        public AddressAppService(IAddressRepository addressRepository) 
            : base(addressRepository)
        {
            _addressRepository = addressRepository;
        }

        public IEnumerable<string> GetNamesLocality(string query)
        {
            var queryable = _addressRepository.GetQueryable();

            if (!string.IsNullOrEmpty(query))
            {
                queryable = queryable.Where(w => w.City.StartsWith(query));
            }

            var result = queryable.Select(s => s.City);

            return result;
        }

        public Address Get(int addressId, int userId)
        {
            return _addressRepository.GetQueryable()
                .FirstOrDefault(f => f.AddressId == addressId && f.PersonId == userId);
        }
    }
}
