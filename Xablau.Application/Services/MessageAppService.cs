﻿using Xablau.Application.Interfaces;
using Xablau.Domain.Entities;
using Xablau.Domain.Repositories;

namespace Xablau.Application.Services
{
    public class MessageAppService : AppServiceBase<Message>, IMessageAppService
    {
        private readonly IMessageRepository _messageRepository;

        public MessageAppService(IMessageRepository messageRepository) 
            : base(messageRepository)
        {
            _messageRepository = messageRepository;
        }
    }
}
