﻿using Xablau.Application.Interfaces;
using Xablau.Domain.Entities;
using Xablau.Domain.Repositories;

namespace Xablau.Application.Services
{
    public class PersonAppService : AppServiceBase<Person>, IPersonAppService
    {

        private readonly IPersonRepository _personRepository;

        public PersonAppService(IPersonRepository personRepository)
            :base (personRepository)
        {
            _personRepository = personRepository;
        }

    }
}
