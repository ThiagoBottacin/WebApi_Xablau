﻿using Xablau.Application.Interfaces;
using Xablau.Domain.Entities;
using Xablau.Domain.Repositories;

namespace Xablau.Application.Services
{
    public class PhoneAppService : AppServiceBase<Phone>, IPhoneAppService
    { 

    private readonly IPhoneRepository _phoneRepository;

    public PhoneAppService(IPhoneRepository phoneRepository) 
        : base(phoneRepository)
        {
            _phoneRepository = phoneRepository;
        }

    }
}
