﻿using Xablau.Application.Interfaces;
using Xablau.Domain.Entities;
using Xablau.Domain.Repositories;

namespace Xablau.Application.Services
{
    public class TagTypeAppService : AppServiceBase<TagType>, ITagTypeAppService
    {
        private readonly ITagTypeRepository _tagtypeRepository;

        public TagTypeAppService(ITagTypeRepository tagtypeRepository) 
            : base(tagtypeRepository)
        {
            _tagtypeRepository = tagtypeRepository;
        }
    }
}
