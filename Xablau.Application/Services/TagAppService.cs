﻿using System.Collections.Generic;
using System.Linq;
using Xablau.Application.Interfaces;
using Xablau.Domain.Entities;
using Xablau.Domain.Repositories;

namespace Xablau.Application.Services
{
    public class TagAppService : AppServiceBase<Tag>, ITagAppService
    {
        private readonly ITagRepository _tagRepository;

        public TagAppService(ITagRepository tagRepository) 
            : base(tagRepository)
        {
            _tagRepository = tagRepository;
        }

        public void InsertAll(IEnumerable<string> tags)
        {
            //Insere Tags
            foreach (var tag in tags)
            {
                var hasTag = _tagRepository.GetQueryable(g => g.Name == tag).FirstOrDefault() != null;

                if (!hasTag)
                {
                    Tag newTag = new Tag
                    {
                        Name = tag
                    };

                    _tagRepository.Add(newTag);
                }
            }
        }
    }
}
