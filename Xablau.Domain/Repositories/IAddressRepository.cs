﻿using Xablau.Domain.Entities;

namespace Xablau.Domain.Repositories
{
    public interface IAddressRepository : IRepositoryBase<Address>
    {
    }
}
