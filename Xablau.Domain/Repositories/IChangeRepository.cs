﻿using Xablau.Domain.Entities;

namespace Xablau.Domain.Repositories
{
    public interface IChangeRepository : IRepositoryBase<Change>
    {
        void Match();
    }
}
