﻿using Xablau.Domain.Entities;

namespace Xablau.Domain.Repositories
{
    public interface ITagRepository : IRepositoryBase<Tag>
    {
    }
}
