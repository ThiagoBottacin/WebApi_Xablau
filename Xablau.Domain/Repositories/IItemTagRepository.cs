﻿using Xablau.Domain.Entities;

namespace Xablau.Domain.Repositories
{
    public interface IItemTagRepository : IRepositoryBase<ItemTag>
    {
    }
}
