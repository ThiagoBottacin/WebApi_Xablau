﻿using System.Collections.Generic;
using Xablau.Domain.Entities;

namespace Xablau.Domain.Repositories
{
    public interface IItemRepository : IRepositoryBase<Item>
    {

        IEnumerable<Item> GetInterestedItems(int userId);
    }
}
