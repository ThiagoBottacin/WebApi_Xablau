﻿using Xablau.Domain.Entities;

namespace Xablau.Domain.Repositories
{
    public interface IPictureRepository : IRepositoryBase<Picture>
    {
    }
}
