﻿using Xablau.Domain.Entities;

namespace Xablau.Domain.Repositories
{
    public interface IMessageRepository : IRepositoryBase<Message>
    {
    }
}
