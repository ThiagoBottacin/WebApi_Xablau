﻿using System;

namespace Xablau.Domain.Entities
{
    public class Change
    {
        public int ChangeId { get; set; }

        public int Item1Id { get; set; }

        public int Item2Id { get; set; }

        public decimal? Price_1 { get; set; }

        public decimal? Price_2 { get; set; }

        public bool? Status { get; set; }

        public bool? Like1 { get; set; }

        public bool? Like2 { get; set; }

        public DateTime DataSave { get; set; }

        public DateTime? DataDelete { get; set; }

        public virtual Item Item1 { get; set; }
        public virtual Item Item2 { get; set; }
    }
}