﻿using System;

namespace Xablau.Domain.Entities
{
    public class Item
    {
        public int ItemId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public decimal? Price { get; set; }

        public DateTime DataSave { get; set; }

        public DateTime? DataDelete { get; set; }

        public virtual Person Person { get; set; }

        public int PersonId { get; set; }

    }
}
