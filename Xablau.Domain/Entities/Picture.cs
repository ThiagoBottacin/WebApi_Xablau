﻿using System;

namespace Xablau.Domain.Entities
{
    public class Picture
    {
        public int PictureId { get; set; }

        public string Image { get; set; }

        public DateTime DataSave { get; set; }

        public DateTime? DataDelete { get; set; }

        public virtual Item Item { get; set; }

        public int ItemId { get; set; }
        
    }
}
