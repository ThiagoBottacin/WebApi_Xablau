﻿using System;
using System.Collections.Generic;

namespace Xablau.Domain.Entities.DTO
{
    public class ItemResult
    {
        public int? ChangeId { get; set; }

        public int ItemId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public decimal? Price { get; set; }

        public DateTime? DataSave { get; set; }

        public DateTime? DataDelete { get; set; }

        public string[] UrlPhotos { get; set; }

        public string[] WishTag { get; set; }

        public string[] DescriptionTag { get; set; }

        public string Address { get; set; }

        public List<Photo> Photos { get; set; }

        public int? TotRegisters { get; set; }
    }

    //Class Auxiliar
    public class Photo
    {
        public int? PhotoId { get; set; }
        public string Name { get; set; }
        public long Size { get; set; }
        public string UrlPhoto { get; set; }
    }
}