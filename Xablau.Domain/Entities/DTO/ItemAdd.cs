﻿using System.Web;

namespace Xablau.Domain.Entities.DTO
{
    public class ItemAdd
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public decimal? Price { get; set; }

        public string[] DescriptionTag { get; set; }

        public string[] WishTag { get; set; }
        public string[] RemovedFiles { get; set; }
    }
}