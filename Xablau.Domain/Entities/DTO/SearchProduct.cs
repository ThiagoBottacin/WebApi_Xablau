﻿namespace Xablau.Domain.Entities.DTO
{
    public class SearchProduct
    {
        public string Name { get; set; }
        public string Locality { get; set; }
    }
}
