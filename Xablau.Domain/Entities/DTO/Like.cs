﻿namespace Xablau.Domain.Entities.DTO
{
    public class Like
    {
        public int? ChangeId { get; set; }
        public int ProductId { get; set; }
        public bool Confirmation { get; set; }
    }
}
