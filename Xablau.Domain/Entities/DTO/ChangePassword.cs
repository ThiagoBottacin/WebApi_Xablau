﻿namespace Xablau.Domain.Entities.DTO
{
    public class ChangePassword
    {
        public string OldPassword { get; set; }

        public string NewPassword { get; set; }
    }
}
