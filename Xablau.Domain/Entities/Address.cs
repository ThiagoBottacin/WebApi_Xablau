﻿using System;

namespace Xablau.Domain.Entities
{
    public class Address
    {
        public int AddressId { get; set; }

        public string Country { get; set; }

        public string State { get; set; }

        public string City { get; set; }

        public string District { get; set; }

        public string Street { get; set; }

        public string Number { get; set; }

        public string ZipCode { get; set; }

        public DateTime DataSave { get; set; }

        public DateTime? DataDelete { get; set; }

        public virtual Person Person { get; set; }

        public int PersonId { get; set; }

        public Address()
        {
            Country = "Brasil";
            DataSave = DateTime.Now;
            DataDelete = null;
        }
    }
}
