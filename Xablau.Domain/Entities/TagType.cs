﻿namespace Xablau.Domain.Entities
{
    public class TagType
    {
        public int TagTypeId { get; set; }

        public string Name { get; set; }
        
    }
}
