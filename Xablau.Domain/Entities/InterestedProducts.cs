﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xablau.Domain.Entities
{
    public class InterestedProducts
    {

        public int InterestedProductsId { get; set; }

        public int Item1Id { get; set; }

        public int Item2Id { get; set; }

        public virtual Item Item1 { get; set; }

        public virtual Item Item2 { get; set; }
       
    }
}
