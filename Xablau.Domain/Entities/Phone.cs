﻿using System;

namespace Xablau.Domain.Entities
{
    public class Phone
    {
        public int PhoneId { get; set; }

        public string DDD { get; set; }

        public string Number { get; set; }
        
        public DateTime DataSave { get; set; }

        public DateTime? DataDelete { get; set; }

        public virtual Person Person { get; set; }

        public int PersonId { get; set; }
        
    }
}
