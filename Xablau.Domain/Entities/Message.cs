﻿using System;

namespace Xablau.Domain.Entities
{
    public class Message
    {
        public int MessageId { get; set; }

        public string Text { get; set; }

        public DateTime DataSave { get; set; }

        public DateTime? DataDelete { get; set; }

        public int PersonId { get; set; }

        public int ChangeId { get; set; }

        public virtual Person Person { get; set; }

        public virtual Change Change { get; set; }
        
    }
}
