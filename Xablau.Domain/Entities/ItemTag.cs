﻿namespace Xablau.Domain.Entities
{
    public class ItemTag
    {
        public int ItemTagId { get; set; }

        public int ItemId { get; set; }

        public int TagId { get; set; }

        public int TagTypeId { get; set; }

        public virtual Item Item { get; set; }

        public virtual Tag Tag { get; set; }

        public virtual TagType TagType { get; set; }
   
    }

}
