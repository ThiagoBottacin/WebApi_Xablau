﻿using System;

namespace Xablau.Domain.Entities
{
    public class Person
    {
        public int PersonId { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public DateTime DtBorn { get; set; }

        public string Photo { get; set; }

        public DateTime DataSave { get; set; }

        public DateTime? DataDelete { get; set; }

        public Person()
        {
            DataSave = DateTime.Now;
            DataDelete = null;
            Photo = "imagem";
        }
    }
}
